export SHAKTITOOLS=/opt/shakti-tools
export RISCV=/opt/riscv/OpenOCD
export PATH="$SHAKTITOOLS/bin:$SHAKTITOOLS/riscv64/bin:$SHAKTITOOLS/riscv64/riscv64-unknown-elf/bin:$RISCV/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games"
