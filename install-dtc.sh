cd /tmp
tar -xvzf dtc-1.4.7.tar.gz
cd dtc-1.4.7/
make NO_PYTHON=1 PREFIX=/usr/
make install NO_PYTHON=1 PREFIX=/usr/
cd /tmp
rm -rf dtc-1.4.7
