#!/bin/bash
XILDIR=/opt/Xilinx/Vivado/2018.3
SHAKTI_TOOLS=/opt/shakti-tools
SHAKTI_SDK=/opt/shakti-sdk
docker run -it --rm \
	-v $HOME:/home/$USER \
	-v /etc/passwd:/etc/passwd:ro \
	-v /etc/shadow:/etc/shadow:ro \
	-v $XILDIR:$XILDIR \
	-v $SHAKTI_TOOLS:$SHAKTI_TOOLS \
	-v $SHAKTI_SDK:$SHAKTI_SDK \
	--user $(id -u):$(id -g) \
	-w $HOME \
	nchandra75/shakti-shell

