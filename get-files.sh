#!/bin/bash
echo "Retrieving files for the Shakti SDK install. "
echo "This requires at least 1GB of space in the /tmp folder"
echo "and at least 3GB of free space in /opt"
echo "- Note: this command uses 'sudo' to unpack the files, so you may"
echo "be asked for your password."

# Get Shakti tools
# TODO: Add some checks here to see if already installed etc.
cd /tmp
wget -O shakti-tools.tgz https://drive.google.com/file/d/15xRqY1RLyzpVi2yPTt5LZVyIz1y-sXIk/view?usp=sharing
wget -O shakti-sdk.tgz   https://drive.google.com/file/d/17RGWN1-PLK4fih1uQ0BnJ8_0RTHhk_qd/view?usp=sharing

# Unpack in /opt
cd /opt
sudo tar xvzf /tmp/shakti-tools.tgz
sudo tar xvzf /tmp/shakti-sdk.tgz

