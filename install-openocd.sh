cd /tmp
git clone https://github.com/riscv/riscv-openocd.git
cd riscv-openocd
./bootstrap
export RISCV=/opt/riscv/OpenOCD
./configure --prefix=$RISCV --enable-remote-bitbang --enable-ftdi --enable-jlink --enable-jtag_vpi --disable-werror
make
make install

