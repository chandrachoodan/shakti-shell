# Docker file for Shakti compilers and SDK
FROM ubuntu:18.04
ENV DEBIAN_FRONTEND="noninteractive"
ENV TZ="Asia/Kolkata"

# Install dependencies from user_manual sec. 3.3.2 (Tool installation)
# First is for the Bluespec compiler

RUN apt-get update && apt-get install -y \
    ghc libghc-regex-compat-dev libghc-syb-dev iverilog \
    libghc-old-time-dev libfontconfig1-dev libx11-dev \
    libghc-split-dev libxft-dev flex bison libxft-dev \
    tcl-dev tk-dev libfontconfig1-dev libx11-dev gperf \
    itcl3-dev itk3-dev autoconf git

RUN git clone --recursive https://github.com/B-Lang-org/bsc && \
    cd bsc && \
    make PREFIX=/opt

RUN apt-get install -y \
    python3 \
    python3-pip \
    python3-serial

RUN apt-get install -y autoconf automake autotools-dev curl make-guile \
    libmpc-dev libmpfr-dev libgmp-dev libusb-1.0-0-dev bc \
    gawk build-essential bison flex texinfo gperf libtool \
    patchutils zlib1g-dev pkg-config libexpat-dev \
    libusb-0.1 libftdi1 libftdi1-2 \
    libpython3.6-dev 

COPY dtc-1.4.7.tar.gz install-dtc.sh install-openocd.sh /tmp/
RUN cd /tmp && /bin/bash ./install-dtc.sh
RUN cd /tmp && /bin/bash ./install-openocd.sh
COPY install-shakti.sh /tmp
RUN cd /tmp && /bin/bash ./install-shakti.sh
COPY install-verilator.sh /tmp
RUN cd /tmp && /bin/bash ./install-verilator.sh

RUN apt-get install -y libmpc3 libmpfr6 sudo

COPY bash.bashrc /etc/

RUN ln -s /usr/lib/x86_64-linux-gnu/libmpfr.so.6 /usr/lib/x86_64-linux-gnu/libmpfr.so.4

RUN pip3 install repomanager==1.2.1 Cerberus==1.3.1 ruamel.yaml==0.15.97

